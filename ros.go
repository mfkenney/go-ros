// Ros provides an interface to ROS rotators.
package ros

import (
	"context"
	"fmt"
	"io"
	"math"
	"sync"
	"time"

	"github.com/pkg/errors"
)

// Limits holds the Rotator user limit values
type Limits struct {
	// Clockwise rotation limit in degrees
	Cw float32 `toml:"cw" json:"cw"`
	// Counter-clockwise rotation limit in degrees
	Ccw float32 `toml:"ccw" json:"ccw"`
}

func (l Limits) String() string {
	return fmt.Sprintf("ccw=%1.f cw=%.1f", l.Ccw, l.Cw)
}

// Motion holds the Rotator motion parameters
type Motion struct {
	// Maximum velocity in deg/s
	Vmax float32 `toml:"vmax" json:"vmax"`
	// Acceleration in deg/s^2
	Accel float32 `toml:"accel" json:"accel"`
	// Brake setting (counts)
	Brake int `toml:"brake" json:"brake"`
}

// Node represents a generic ROS device
type Node interface {
	// Send writes a command to an ROS node and returns the optional
	// response. Nresp is the number of response characters expected.
	Send(command string, nresp int) (string, error)
}

// Rotator represents an ROS positioner
type Rotator interface {
	Node
	// Return the positioner factory settings
	Settings() RotatorSettings
	// Set rotation limits
	SetLimits(l Limits) error
	// Get rotation limits
	Limits() Limits
	// Set the motion parameters
	SetMotion(m Motion) error
	// Get motion parameters
	Motion() (Motion, error)
	// Get the current angular position in degrees
	Position() (float32, error)
	// Start a move relative to the current position
	Move(ctx context.Context, degrees float32, useSteps bool) error
	// Return a channel which will provide the current position at the specified
	// interval while the rotator is moving.
	Monitor(ctx context.Context, interval time.Duration) <-chan float32
	// Apply the brake with an optional ramp-down
	Stop(brake int, rampDown bool) error
	// Return true if the rotator is moving
	IsMoving() bool
	// Return true if the rotator has stalled
	HasStalled() bool
}

type DevType int

const (
	Positioner10_88 DevType = iota + 1
	Positioner25_160
	Camera
	Light
	Positioner10_50
)

// Empirically determined rotator step size.
const stepSize = 0.0056296358

type RotatorSettings struct {
	FactoryLimits [2]int
	UserLimits    [2]int
	PcbDash       int
	Feedback      bool
	PcbSn         int
	Type          DevType
	FirmwareRev   int
}

type rosNode struct {
	port io.ReadWriter
	id   string
	mu   *sync.Mutex
}

type rosRotator struct {
	rosNode
	settings RotatorSettings
	params   Motion
}

func sendBytes(port io.ReadWriter, buf []byte) error {
	resp := make([]byte, 1)
	for i := 0; i < len(buf); i++ {
		_, err := port.Write(buf[i : i+1])
		if err != nil {
			return err
		}

		for {
			n, _ := port.Read(resp)
			if n == 0 {
				return errors.New("Timeout on character echo")
			}
			if resp[0] == buf[i] {
				break
			}
		}
	}
	return nil
}

func NewRotator(port io.ReadWriter, id string) Rotator {
	r := &rosRotator{
		rosNode: rosNode{port: port, id: id, mu: &sync.Mutex{}},
	}

	resp, err := r.Send("?000", 33)
	if err == nil && len(resp) > 0 {
		r.settings = r.parseSettings(resp)
	}

	return r
}

func (rn *rosNode) Send(command string, nresp int) (string, error) {
	reply := make([]byte, nresp)
	rn.mu.Lock()
	defer rn.mu.Unlock()

	err := sendBytes(rn.port, []byte(rn.id+command))
	if err != nil {
		return "", err
	}
	if nresp > 0 {
		_, err = io.ReadFull(rn.port, reply)
	}

	return string(reply), err
}

func (r *rosRotator) parseSettings(s string) RotatorSettings {
	rs := RotatorSettings{}
	var (
		ignore_s, fb string
		ignore_i     int
	)

	_, err := fmt.Sscanf(s, "%1s,%03d,%03d,%03d,%03d,%d,%1s,%04d,%d,%d,%02d",
		&ignore_s,
		&rs.FactoryLimits[0], &rs.FactoryLimits[1],
		&rs.UserLimits[0], &rs.UserLimits[1],
		&rs.PcbDash,
		&fb,
		&rs.PcbSn,
		&ignore_i,
		&rs.Type,
		&rs.FirmwareRev)
	if err == nil {
		rs.Feedback = fb == "y"
	}

	return rs
}

func (r *rosRotator) countsToDegrees(counts int) float32 {
	drange := float32(r.settings.FactoryLimits[1] - r.settings.FactoryLimits[0])
	return 360. * float32(counts-r.settings.FactoryLimits[0]) / drange
}

func (r *rosRotator) degreesToCounts(degrees float32) int {
	scale := degrees / 360.0
	drange := float32(r.settings.FactoryLimits[1] - r.settings.FactoryLimits[0])
	return int(scale*drange + float32(r.settings.FactoryLimits[0]) + 0.5)
}

func (r *rosRotator) degreesToSteps(degrees float32) int {
	return int(math.Ceil(float64(degrees) / stepSize))
}

func degreesPerSecond(cps int) float32 {
	return float32(cps) * 0.5
}

func countsPerSecond(dps float32) int {
	return int(dps * 2.0)
}

func (r *rosRotator) Settings() RotatorSettings {
	return r.settings
}

func (r *rosRotator) Limits() Limits {
	return Limits{
		Ccw: r.countsToDegrees(r.settings.UserLimits[0]),
		Cw:  r.countsToDegrees(r.settings.UserLimits[1]),
	}
}

func (r *rosRotator) SetLimits(l Limits) error {
	var v [2]int
	v[0] = r.degreesToCounts(l.Ccw)
	v[1] = r.degreesToCounts(l.Cw)

	// Sanity checks
	if v[0] < r.settings.FactoryLimits[0] || v[0] > r.settings.FactoryLimits[1] {
		return errors.Errorf("Invalid CCW limit; %d", v[0])
	}
	if v[1] < r.settings.FactoryLimits[0] || v[1] > r.settings.FactoryLimits[1] {
		return errors.Errorf("Invalid CW limit; %d", v[1])
	}
	if v[0] >= v[1] {
		return errors.Errorf("Limit range error; %d >= %d", v[0], v[1])
	}

	// Set CCW limit
	_, err := r.Send(fmt.Sprintf("d%03d", v[0]), 0)
	if err != nil {
		return errors.Wrap(err, "set CCW limit")
	}
	r.settings.UserLimits[0] = v[0]

	// Set CW limit
	_, err = r.Send(fmt.Sprintf("u%03d", v[1]), 0)
	if err != nil {
		return errors.Wrap(err, "set CW limit")
	}
	r.settings.UserLimits[1] = v[1]

	return nil
}

func (r *rosRotator) getIntValue(query int) (int, error) {
	cmd := fmt.Sprintf("?%03d", query)
	resp, err := r.Send(cmd, 4)
	if err != nil {
		return 0, errors.Wrapf(err, "command: %q", cmd)
	}

	var (
		val       int
		ignored_s string
	)
	_, err = fmt.Sscanf(resp, "%1s%03d", &ignored_s, &val)
	return val, err
}

func (r *rosRotator) getMaxVel() (int, error) {
	return r.getIntValue(4)
}

func (r *rosRotator) getAccel() (int, error) {
	return r.getIntValue(3)
}

func (r *rosRotator) getBrake() (int, error) {
	return r.getIntValue(6)
}

func (r *rosRotator) Motion() (Motion, error) {
	if r.params.Brake != 0 {
		return r.params, nil
	}

	m := Motion{}

	val, err := r.getMaxVel()
	if err != nil {
		return m, err
	}
	m.Vmax = degreesPerSecond(val)

	val, err = r.getAccel()
	if err != nil {
		return m, err
	}
	m.Accel = float32(val+1.0) * 2.0

	m.Brake, err = r.getBrake()
	return m, err
}

func (r *rosRotator) SetMotion(m Motion) error {
	val := countsPerSecond(m.Vmax)
	if val <= 0 || val > 80 {
		return errors.Errorf("Invalid speed value; %f", m.Vmax)
	}
	_, err := r.Send(fmt.Sprintf("m%03d", val), 0)
	if err != nil {
		return errors.Wrap(err, "set maxvel")
	}

	val = int(m.Accel/2.0 - 1.0)
	if val < 0 || val > 4 {
		return errors.Errorf("Invalid acceleration value; %f", m.Vmax)
	}
	_, err = r.Send(fmt.Sprintf("a%03d", val), 0)
	if err != nil {
		return errors.Wrap(err, "set accel")
	}

	err = r.Stop(m.Brake, false)
	if err != nil {
		return errors.Wrap(err, "set brake")
	}

	r.params = m
	return nil
}

func (r *rosRotator) Position() (float32, error) {
	resp, err := r.Send("g", 4)
	if err != nil {
		return 0.0, errors.Wrap(err, "get angle")
	}

	var (
		counts    int
		ignored_s string
	)
	_, err = fmt.Sscanf(resp, "%1s%03d", &ignored_s, &counts)

	return r.countsToDegrees(counts), err
}

func absValue(x float32) float32 {
	return float32(math.Abs(float64(x)))
}

func (r *rosRotator) Move(ctx context.Context, degrees float32, useSteps bool) error {
	var (
		cmd string
	)

	if useSteps {
		target := r.degreesToSteps(absValue(degrees))
		speed, err := r.getMaxVel()
		if err != nil {
			return err
		}
		if degrees < 0 {
			cmd = fmt.Sprintf("y0%02d%05d", speed, target)
		} else {
			cmd = fmt.Sprintf("y1%02d%05d", speed, target)
		}
	} else {
		pos, err := r.Position()
		if err != nil {
			return err
		}
		target := r.degreesToCounts(pos + degrees)
		cmd = fmt.Sprintf("p%03d", target)
	}

	_, err := r.Send(cmd, 0)
	return err
}

func (r *rosRotator) Monitor(ctx context.Context, interval time.Duration) <-chan float32 {
	ch := make(chan float32, 1)
	brake, _ := r.getBrake()

	go func() {
		defer close(ch)
		for r.IsMoving() {
			select {
			case <-ctx.Done():
				r.Stop(brake, false)
				return
			case <-time.After(interval):
				x, _ := r.Position()
				ch <- x
			}
		}
	}()

	return ch
}

func (r *rosRotator) Stop(brake int, rampDown bool) error {
	var cmd string
	if rampDown {
		cmd = fmt.Sprintf("t%03d", brake)
	} else {
		cmd = fmt.Sprintf("s%03d", brake)
	}
	_, err := r.Send(cmd, 0)
	if err != nil {
		return errors.Wrap(err, "set brake")
	}
	return nil
}

func (r *rosRotator) IsMoving() bool {
	val, _ := r.getIntValue(7)
	return val == 1
}

func (r *rosRotator) HasStalled() bool {
	val, _ := r.getIntValue(5)
	return val == 1
}
