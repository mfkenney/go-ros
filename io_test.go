package ros

import (
	"bytes"
	"testing"
)

type mockPort struct {
	rwbuf, rbuf bytes.Buffer
}

func (mp *mockPort) Write(p []byte) (int, error) {
	mp.rwbuf.Write(p)
	n, _ := mp.rbuf.Write(p)
	if mp.rbuf.Len() == 2 && (p[0] == 'f' || p[0] == 'g') {
		mp.rwbuf.WriteString("A042")
		mp.rbuf.Reset()
	}

	if mp.rbuf.Len() == 5 {
		// Load command response into rwbuf
		cmd := mp.rbuf.String()
		switch cmd[1] {
		case '?':
			switch cmd[2:] {
			case "000":
				mp.rwbuf.WriteString("A,010,989,015,975,2,y,0007,2,1,03")
			case "003":
				mp.rwbuf.WriteString("A001")
			case "004":
				mp.rwbuf.WriteString("A002")
			case "006":
				mp.rwbuf.WriteString("A089")
			}
		case 'u':
		case 'd':
		case 'm':
		case 'a':
		case 's':
		case 't':
		}
		// Reset rbuf
		mp.rbuf.Reset()
	}

	return n, nil
}

func (mp *mockPort) Read(p []byte) (int, error) {
	return mp.rwbuf.Read(p)
}

func TestSettings(t *testing.T) {
	mp := &mockPort{}
	r := NewRotator(mp, "A")
	s := r.Settings()
	if s.UserLimits[0] != 15 {
		t.Errorf("Bad user limit; expected 15 got %d", s.UserLimits[0])
	}
}

func TestMotion(t *testing.T) {
	mp := &mockPort{}
	r := NewRotator(mp, "A")
	m, _ := r.Motion()
	expected := degreesPerSecond(2)
	if m.Vmax != expected {
		t.Errorf("Bad maxvel; expected %.1f, got %.1f", expected, m.Vmax)
	}

	if m.Brake != 89 {
		t.Errorf("Bad brake value; expected 89, got %d", m.Brake)
	}
}
